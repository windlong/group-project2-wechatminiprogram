package com.cskaoyan.bean.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 角色表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketRole {
    private Integer id;

    /**
    * 角色名称
    */
    private String name;

    /**
    * 角色描述
    */
    private String desc;

    /**
    * 是否启用
    */
    private Boolean enabled;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}