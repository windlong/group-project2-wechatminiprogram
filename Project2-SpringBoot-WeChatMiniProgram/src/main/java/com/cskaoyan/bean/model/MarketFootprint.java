package com.cskaoyan.bean.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 用户浏览足迹表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketFootprint {
    private Integer id;

    /**
    * 用户表的用户ID
    */
    private Integer userId;

    /**
    * 浏览商品ID
    */
    private Integer goodsId;

    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")

    private Date addTime;

    /**
    * 更新时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")

    private Date updateTime;

    /**
    * 逻辑删除
    */

    private Boolean deleted;
}