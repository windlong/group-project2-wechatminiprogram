package com.cskaoyan.bean.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 专题表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketTopic {
    private Integer id;

    /**
    * 专题标题
    */
    private String title;

    /**
    * 专题子标题
    */
    private String subtitle;

    /**
    * 专题内容，富文本格式
    */
    private String content;

    /**
    * 专题相关商品最低价
    */
    private BigDecimal price;

    /**
    * 专题阅读量
    */
    private String readCount;

    /**
    * 专题图片
    */
    private String picUrl;

    /**
    * 排序
    */
    private Integer sortOrder;

    /**
    * 专题相关商品，采用JSON数组格式
    */
    private String[] goods;

    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
    * 更新时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}