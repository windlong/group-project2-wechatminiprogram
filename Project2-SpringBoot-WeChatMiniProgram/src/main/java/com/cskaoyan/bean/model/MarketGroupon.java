package com.cskaoyan.bean.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 团购活动表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketGroupon {
    private Integer id;

    /**
    * 关联的订单ID
    */
    private Integer orderId;

    /**
    * 如果是开团用户，则groupon_id是0；如果是参团用户，则groupon_id是团购活动ID
    */
    private Integer grouponId;

    /**
    * 团购规则ID，关联market_groupon_rules表ID字段
    */
    private Integer rulesId;

    /**
    * 用户ID
    */
    private Integer userId;

    /**
    * 团购分享图片地址
    */
    private String shareUrl;

    /**
    * 开团用户ID
    */
    private Integer creatorUserId;

    /**
    * 开团时间
    */
    private Date creatorUserTime;

    /**
    * 团购活动状态，开团未支付则0，开团中则1，开团失败则2
    */
    private Short status;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}