package com.cskaoyan.bean.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 通知管理员表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketNoticeAdmin {
    private Integer id;

    /**
    * 通知ID
    */
    private Integer noticeId;

    /**
    * 通知标题
    */
    private String noticeTitle;

    /**
    * 接收通知的管理员ID
    */
    private Integer adminId;

    /**
    * 阅读时间，如果是NULL则是未读状态
    */
    private Date readTime;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}