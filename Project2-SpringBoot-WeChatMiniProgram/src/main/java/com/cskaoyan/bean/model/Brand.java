package com.cskaoyan.bean.model;

import lombok.Data;

/**
 * @Author：刘运轩
 * @Package：com.cskaoyan.bean.model
 * @Project：group-project-2
 * @name：Brand
 * @Date：2024/4/14 21:16
 * @Filename：Brand
 */
@Data
public class Brand {
    private Integer value;

    private String label;
}
