package com.cskaoyan.bean.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 意见反馈表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketFeedback {
    private Integer id;

    /**
    * 用户表的用户ID
    */
    private Integer userId;

    /**
    * 用户名称
    */
    private String username;

    /**
    * 手机号
    */
    private String mobile;

    /**
    * 反馈类型
    */
    private String feedType;

    /**
    * 反馈内容
    */
    private String content;

    /**
    * 状态
    */
    private Integer status;

    /**
    * 是否含有图片
    */
    private Boolean hasPicture;

    /**
    * 图片地址列表，采用JSON数组格式
    */
    private String picUrls;

    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")

    private Date addTime;

    /**
    * 更新时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")

    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}