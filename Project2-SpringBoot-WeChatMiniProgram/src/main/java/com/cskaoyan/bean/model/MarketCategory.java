package com.cskaoyan.bean.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 类目表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketCategory {
    private Integer id;

    /**
    * 类目名称
    */
    private String name;

    /**
    * 类目关键字，以JSON数组格式
    */
    private String keywords;

    /**
    * 类目广告语介绍
    */
    private String desc;

    /**
    * 父类目ID
    */
    private Integer pid;

    /**
    * 类目图标
    */
    private String iconUrl;

    /**
    * 类目图片
    */
    private String picUrl;

    private String level;

    /**
    * 排序
    */
    private Byte sortOrder;

    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
    * 更新时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;

    private List<MarketCategory> children;
}