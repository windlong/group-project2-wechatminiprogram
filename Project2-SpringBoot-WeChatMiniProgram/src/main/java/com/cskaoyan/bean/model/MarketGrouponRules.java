package com.cskaoyan.bean.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 团购规则表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketGrouponRules {
    private Integer id;

    /**
    * 商品表的商品ID
    */
    private Integer goodsId;

    /**
    * 商品名称
    */
    private String goodsName;

    /**
    * 商品图片或者商品货品图片
    */
    private String picUrl;

    /**
    * 优惠金额
    */
    private BigDecimal discount;

    /**
    * 达到优惠条件的人数
    */
    private Integer discountMember;

    /**
    * 团购过期时间
    */
    private Date expireTime;

    /**
    * 团购规则状态，正常上线则0，到期自动下线则1，管理手动下线则2
    */
    private Short status;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}