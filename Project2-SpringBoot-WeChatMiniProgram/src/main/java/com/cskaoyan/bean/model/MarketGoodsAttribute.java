package com.cskaoyan.bean.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 商品参数表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketGoodsAttribute {
    private Integer id;

    /**
    * 商品表的商品ID
    */
    private Integer goodsId;

    /**
    * 商品参数名称
    */
    private String attribute;

    /**
    * 商品参数值
    */
    private String value;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}