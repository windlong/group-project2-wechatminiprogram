package com.cskaoyan.bean.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 关键字表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketKeyword {
    private Integer id;

    /**
    * 关键字
    */
    private String keyword;

    /**
    * 关键字的跳转链接
    */
    private String url;

    /**
    * 是否是热门关键字
    */
    private Boolean isHot;

    /**
    * 是否是默认关键字
    */
    private Boolean isDefault;

    /**
    * 排序
    */
    private Integer sortOrder;

    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
    * 更新时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}