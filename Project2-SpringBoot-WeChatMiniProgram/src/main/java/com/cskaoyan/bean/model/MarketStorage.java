package com.cskaoyan.bean.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 文件存储表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketStorage {
    private Integer id;

    /**
    * 文件的唯一索引
    */
    private String key;

    /**
    * 文件名
    */
    private String name;

    /**
    * 文件类型
    */
    private String type;

    /**
    * 文件大小
    */
    private Integer size;

    /**
    * 文件访问链接
    */
    private String url;

    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
    * 更新时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}