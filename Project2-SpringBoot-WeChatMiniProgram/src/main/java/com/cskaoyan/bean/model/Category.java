package com.cskaoyan.bean.model;

import lombok.Data;

import java.util.List;

/**
 * @Author：刘运轩
 * @Package：com.cskaoyan.bean.model
 * @Project：group-project-2
 * @name：Category
 * @Date：2024/4/14 21:16
 * @Filename：Category
 */
@Data
public class Category {
    private Integer value;

    private String label;

    private List<Child> children;
    @Data
    public static class Child{
        private Integer value;

        private String label;
    }
}
