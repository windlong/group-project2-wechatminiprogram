package com.cskaoyan.bean.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 用户表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketUser {
    private Integer id;

    /**
    * 用户名称
    */
    private String username;

    /**
    * 用户密码
    */
    private String password;

    /**
    * 性别：0 未知， 1男， 1 女
    */
    private Byte gender;

    /**
    * 生日
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;

    /**
    * 最近一次登录时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;

    /**
    * 最近一次登录IP地址
    */
    private String lastLoginIp;

    /**
    * 0 普通用户，1 VIP用户，2 高级VIP用户
    */
    private Byte userLevel;

    /**
    * 用户昵称或网络名称
    */
    private String nickname;

    /**
    * 用户手机号码
    */
    private String mobile;

    /**
    * 用户头像图片
    */
    private String avatar;

    /**
    * 微信登录openid
    */
    private String weixinOpenid;

    /**
    * 微信登录会话KEY
    */
    private String sessionKey;

    /**
    * 0 可用, 1 禁用, 2 注销
    */
    private Byte status;

    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
    * 更新时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}