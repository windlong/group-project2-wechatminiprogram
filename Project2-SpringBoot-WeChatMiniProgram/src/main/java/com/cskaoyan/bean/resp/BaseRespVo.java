package com.cskaoyan.bean.resp;

import lombok.Data;

@Data
public class BaseRespVo<T> {
    private T data;
    private String errmsg;
    private int errno;

    public static <T> BaseRespVo ok(T data) {
        BaseRespVo<T> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg("成功");
        baseRespVo.setData(data);
        return baseRespVo;
    }
    public static <T> BaseRespVo msg(String errmsg) {
        BaseRespVo<T> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg(errmsg);
        return baseRespVo;
    }
    public static <T> BaseRespVo err(int errno,String errmsg) {
        BaseRespVo<T> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(errno);
        baseRespVo.setErrmsg(errmsg);
        return baseRespVo;
    }
    public static <T> BaseRespVo err(String errmsg) {
        BaseRespVo<T> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(500);
        baseRespVo.setErrmsg(errmsg);
        return baseRespVo;
    }

    public static BaseRespVo unAuthen() {
        return msg("请先登录");
    }
}
