package com.cskaoyan.bean.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
public class LoginRespVo {


    private UserInfoDTO userInfo;
    private String token;

    @NoArgsConstructor
    @Data
    @Builder
    @AllArgsConstructor
    public static class UserInfoDTO {
        private String nickName;
        private String avatarUrl;
    }

}
