package com.cskaoyan.bean.resp;

import com.cskaoyan.bean.model.MarketAddress;
import com.cskaoyan.bean.model.MarketCart;
import com.cskaoyan.bean.model.MarketGoods;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
public class CartCheckoutVo {

    private Integer grouponRulesId;
    private BigDecimal actualPrice;
    private BigDecimal orderTotalPrice;
    private Integer cartId;
    private Integer userCouponId;
    private Integer couponId;
    private BigDecimal goodsTotalPrice;
    private Integer addressId;
    private BigDecimal grouponPrice;
    private MarketAddress checkedAddress;
    private BigDecimal couponPrice;
    private Integer availableCouponLength;
    private BigDecimal freightPrice;
    private List<MarketCart> checkedGoodsList;

}
