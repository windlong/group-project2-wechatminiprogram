package com.cskaoyan.bean.resp;

import com.cskaoyan.bean.model.MarketOrder;
import com.cskaoyan.bean.model.MarketOrderGoods;
import lombok.Data;

import java.util.List;

@Data
public class OrderDetailVo {
    MarketOrder order;
    List<MarketOrderGoods> orderGoods;
    UserVo user;
}
