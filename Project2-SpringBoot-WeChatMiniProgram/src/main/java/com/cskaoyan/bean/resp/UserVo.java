package com.cskaoyan.bean.resp;

import lombok.Data;

@Data
public class UserVo {
   String nickname;
   String avatar;
}
