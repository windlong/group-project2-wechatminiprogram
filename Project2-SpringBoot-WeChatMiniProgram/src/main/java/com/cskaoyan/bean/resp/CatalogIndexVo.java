package com.cskaoyan.bean.resp;

import com.cskaoyan.bean.model.MarketCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Joe
 * @since 2024/04/27 19:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CatalogIndexVo extends CurrentCatalogVo{

    private List<MarketCategory> categoryList;
}
