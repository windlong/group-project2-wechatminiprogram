package com.cskaoyan.bean.resp;

import lombok.Data;

import java.util.List;

@Data
public class UserStatVo {
    List<String> columns;
    List<Row> rows;
    @Data
    public static class Row{
        String day;
        Integer users;
    }
}
