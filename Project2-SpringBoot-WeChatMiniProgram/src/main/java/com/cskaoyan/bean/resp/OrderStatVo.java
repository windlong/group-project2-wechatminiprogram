package com.cskaoyan.bean.resp;

import lombok.Data;

import java.util.List;

@Data
public class OrderStatVo {
    List<String> columns;
    List<Row> rows;

    @Data
    public static class Row{
        String day;
        String orders;
        String customers;
        String amount;
        String pcr;
    }
}
