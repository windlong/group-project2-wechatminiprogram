package com.cskaoyan.bean.resp;

import com.cskaoyan.bean.model.MarketCategory;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Joe
 * @since 2024/04/27 18:23
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class CurrentCatalogVo {
        @JsonProperty("currentCategory")
        //当前一级分类
        private MarketCategory currentCategory;

        @JsonProperty("currentSubCategory")
        //当前一级分类下的二级分类
        private List<MarketCategory> currentSubCategory;
}
