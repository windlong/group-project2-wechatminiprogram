package com.cskaoyan.bean.resp;

import com.cskaoyan.bean.model.MarketCart;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
public class CartData {
    List<MarketCart> cartList;
    CartTotal cartTotal;

    public static CartData stat(List<MarketCart> marketCarts) {
        CartData data = new CartData();
        data.setCartList(marketCarts);

        // todo：已修改
        BigDecimal checkedGoodsAmount = marketCarts.stream()
                .filter(MarketCart::getChecked)
                .map(marketCart -> marketCart.getPrice().multiply(new BigDecimal(marketCart.getNumber())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        // todo：已修改
        BigDecimal goodsAmount = marketCarts.stream()
                .map(marketCart -> marketCart.getPrice().multiply(new BigDecimal(marketCart.getNumber())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);


        Short checkedGoodsCount = (short) marketCarts.stream()
                .filter(MarketCart::getChecked)
                .map(MarketCart::getNumber)
                .mapToInt(Short::intValue)
                .sum();
        Short goodsCount = (short) marketCarts.stream()
                .map(MarketCart::getNumber)
                .mapToInt(Short::intValue)
                .sum();

        CartTotal total = CartTotal.builder() // 静态内容类
                .checkedGoodsAmount(checkedGoodsAmount) // 静态内部类的成员变量赋值
                .checkedGoodsCount(checkedGoodsCount) // 静态内部类的成员变量赋值
                .goodsAmount(goodsAmount) // 静态内部类的成员变量赋值
                .goodsCount(goodsCount) // 静态内部类的成员变量赋值
        // 使用到CartTotal的有参构造方法 → 如果要使用建造者构造实例 → 需要增加有参构造方法，增加有参构造方法同时增加上无参构造
                .build();
        data.setCartTotal(total);
        return data;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CartTotal {
        private Short goodsCount;
        private BigDecimal goodsAmount;
        private Short checkedGoodsCount;
        private BigDecimal checkedGoodsAmount;
    }
}
