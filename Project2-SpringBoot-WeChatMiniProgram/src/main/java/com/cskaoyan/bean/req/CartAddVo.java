package com.cskaoyan.bean.req;

import lombok.Data;

@Data
public class CartAddVo {
    private Integer goodsId;
    private Short number;
    private Integer productId;

}
