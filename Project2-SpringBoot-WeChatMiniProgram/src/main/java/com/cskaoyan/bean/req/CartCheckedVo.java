package com.cskaoyan.bean.req;

import lombok.Data;

import java.util.List;

@Data
public class CartCheckedVo {
    // 0和1 也可以使用布尔类型的值来接收
    // 这里使用Boolean → 请求参数的名称 这里以is作为作为开头 → boolean的get方法就是is开头的
    // 做JSON转换的时候它会认为你的名称是checked而不是isChecked
    private Boolean isChecked;
    // [] 可以使用数组也可以使用List → 这里写List主要是为了使用逆向工程的使用 有可能使用in语句
    private List<Integer> productIds;

    /*public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }*/
}
