package com.cskaoyan.bean.req;

import lombok.Data;

import java.util.Date;

@Data
public class OrderPageInfo extends PageInfo{
    /*String sort;   通过继承引入
    String order;
    Integer page;
    Integer limit;*/
    Integer userId;
    String orderSn;
    Date start;
    Date end;
    Short[] orderStatusArray;

}
