package com.cskaoyan.controller;

import com.cskaoyan.bean.resp.BaseRespVo;
import com.cskaoyan.bean.resp.CatalogIndexVo;
import com.cskaoyan.bean.resp.CurrentCatalogVo;
import com.cskaoyan.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信类目控制层
 *
 * @author Joe
 * @since 2024/04/27 18:00
 */
@RestController
@RequestMapping("wx/catalog")
public class WxCatalogController {
    @Autowired
    private CatalogService catalogService;

    @RequestMapping("current")
    public BaseRespVo current(Integer id) {
        //获取当前类目信息
        CurrentCatalogVo currentCatalogVo = catalogService.current(id);
        return BaseRespVo.ok(currentCatalogVo);
    }

    @RequestMapping("index")
    public BaseRespVo index() {
        //获取所有类目信息
        CatalogIndexVo indexVo = catalogService.index();
        if (indexVo == null){
            return BaseRespVo.err("获取类目信息失败");
        }
        return BaseRespVo.ok(indexVo);

    }

}
