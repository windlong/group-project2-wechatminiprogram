package com.cskaoyan.controller;

import com.cskaoyan.bean.resp.BaseRespVo;
import com.cskaoyan.bean.resp.PageVo;
import com.cskaoyan.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("wx/goods")
public class WxGoodsController {
    @Autowired
    private GoodsService goodsService;

    @GetMapping("detail")
    public BaseRespVo detail(Integer id) {
        // 根据id查询data
        Map data = goodsService.detail(id);
        return BaseRespVo.ok(data);
    }

    @GetMapping("count")
    public BaseRespVo count() {
        long count = goodsService.count();
        return BaseRespVo.ok(count);
    }

    @GetMapping("related")
    public BaseRespVo related(Integer id) {
        PageVo data = goodsService.goodsRelated(id);
        return BaseRespVo.ok(data);
    }
}
