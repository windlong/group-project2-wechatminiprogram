package com.cskaoyan.controller;

import com.cskaoyan.bean.resp.BaseRespVo;
import com.cskaoyan.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequestMapping("/wx/home")
@RestController
public class WxHomeController{

    @Autowired
    private IndexService indexService;

    @GetMapping("index")
    public BaseRespVo index(HttpServletRequest request, HttpServletResponse response) {
        Integer userId = (Integer) request.getSession().getAttribute("userId");
        return BaseRespVo.ok(indexService.index(userId));
    }
}
