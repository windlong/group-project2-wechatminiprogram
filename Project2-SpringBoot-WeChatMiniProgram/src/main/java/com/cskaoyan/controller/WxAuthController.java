package com.cskaoyan.controller;

import com.cskaoyan.bean.model.MarketUser;
import com.cskaoyan.bean.resp.BaseRespVo;
import com.cskaoyan.bean.resp.LoginRespVo;
import com.cskaoyan.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("wx/auth")
public class WxAuthController {

    @Autowired
    UserService userService;

    // /admin/auth/login 登录请求在Filter中放行（可以直接访问） → 匿名访问
    @PostMapping("login")
    public BaseRespVo login(@RequestBody Map<String, String> map) {
        // 获取用户名和密码
        String username = map.get("username");
        String password = map.get("password");
        // 登录(认证) → 要使用Shiro来登录 → subject.login(AuthenticationToken)
        // Subject对象的获取是一个固定代码
        Subject subject = SecurityUtils.getSubject();
        // shiro做登录的时候也要用户名和密码 → 根据用户名查询当前用户在系统内的 凭据(密码) ,和请求传入的密码做比较
        AuthenticationToken token = new UsernamePasswordToken(username, password);
        subject.login(token);

        Integer userId = (Integer) subject.getPrincipal();
        MarketUser marketUser = userService.findById(userId);

        LoginRespVo loginRespVo = LoginRespVo.builder()
                .token((String) subject.getSession().getId()) // 结合会话管理器保证使用的是同一个会话 → 把钥匙返回给小程序
                .userInfo(LoginRespVo.UserInfoDTO.builder()
                        .nickName(marketUser.getNickname())
                        .avatarUrl(marketUser.getAvatar())
                        .build())
                .build();


        return BaseRespVo.ok(loginRespVo);
    }

    @GetMapping("unAuth")
    public BaseRespVo unAuthen() {
        return BaseRespVo.unAuthen();
    }
}
