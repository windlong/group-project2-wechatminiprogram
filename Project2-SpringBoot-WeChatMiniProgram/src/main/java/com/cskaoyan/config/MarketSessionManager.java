package com.cskaoyan.config;

import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

// 修改Shiro提供的会话管理器中的获取会话id的方法 → 继承和重写然后再替代你
@Component
public class MarketSessionManager extends DefaultWebSessionManager {

        private static final String TOKEN = "X-CskaoyanMarket-Token";
    @Override
    protected Serializable getSessionId(ServletRequest servletRequest, ServletResponse response) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String sessionId = request.getHeader(TOKEN);
        if (sessionId != null && sessionId.length() > 0) {
            return sessionId;
        }
        return super.getSessionId(servletRequest, response);
    }
}
