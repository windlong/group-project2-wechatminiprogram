package com.cskaoyan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


// 里面包含@ComponentScan → 默认的扫描包配置 → 当前类(启动类)所在的包目录 → 当前应用的扫描包 com.cskaoyan

/**
 * MyBatis的配置
 * 1. 引入其starter依赖(mybatis-spring-boot-starter)
 * 2. 会自动帮我们做组件注册，注册组件过程中需要一些值，这些值使用不了默认值
 *    (1) spring.datasource.xxx
 *    (2) @MapperScan
 * 整合一下分页插件
 * 1. 引入其starter依赖(pagehelper-spring-boot-starter)
 * 2. 提供helper-dialect
 */
@MapperScan("com.cskaoyan.mapper")
@SpringBootApplication
// @EnableAspectJAutoProxy
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
