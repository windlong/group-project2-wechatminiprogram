package com.cskaoyan.util;

public class StringUtil {
    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isNumeric(String idStr) {
        // 判断字符串是否只含有数字
        return idStr.matches("\\d+");
    }

    public static boolean isAllSpaces(String str) {
        if (str == null|| str.isEmpty()) {
            // 如果字符串是null，则返回false
            return false;
        }
        return str.trim().isEmpty();
    }
 //删除字符串中的空格
    public static String cleanString(String str) {
        return str.replaceAll("\\s", "");
    }
}
