package com.cskaoyan.util;

import java.math.BigDecimal;

/**
 * @Description：
 * @Author：BLUEBLANK
 * @Date：2024/4/15 13:59
 */


public class LogicUtil {
    public static boolean isNotPassword(String password) {
        // 密码规范为 数字、字母、特殊字符的任意组合 长度大于等于6
        // 特殊字符 不能为空格
        //先去除空格字符，再判断长度
        password = password.replaceAll("\\s", "");
        // 只需要判断长度即可
        if (password.length() <6) {
            return true;
        }
        return false;
    }

    // 管理员的名字规范
    public static boolean isNotUsername(String username) {
        // 经过测试，管理员名称规范为 数字、字母  长度大于等于6
        if (username.length() < 6) {
            return true;
        }

        if(username.length() > 63){
            return true;
        }
        // 判断字符是否是英文字符


        // 不是数字和字母 返回true
        for (int i = 0; i < username.length(); i++) {
            char c = username.charAt(i);
            if (! (Character.isLetterOrDigit(c)) ){
                return true;
            }
        }

        return false;
    }

    // 手机号规范
    public static boolean isNotPhone(String phone){
        // 手机号规范为 11位数字
        // 手机号号段
        // 130-139、145-149（不包含146）
        // 150-153、155-159
        // 166-167
        // 171-173、175-178、
        // 180-189、 190-191 193 195-199 开头

        // 判断是否是11位的数字
        if (!phone.matches("\\d{11}")) {
            return true;
        }
        // 判断是否是正确的手机号段
        String prefix = phone.substring(0,3);
        if (!prefix.matches("(13[0-9]|14[579]|15[012356789]|16[67]|17[1235678]|18[0123456789]|19[01356789])")) {
            return true;
        }
        return false;
    }


    public static boolean isNotNickname(String nickname) {
        // 昵称不能为空
        // 假设昵称可以重复，不进行重复性判断
        // 限制特殊字符 -- 昵称可以英文、数字、中文，不允许特殊字符

        // ========== 以下方法不能限制特殊字符=====================
        // for (int i = 0; i < nickname.length(); i++) {
        //     char c = nickname.charAt(i);
        //     if (!Character.isLetterOrDigit(c) && Character.isWhitespace(c)) {
        //         return true;
        //     }
        // }

        if (!nickname.matches("[a-zA-Z0-9\\u4e00-\\u9fa5]+")) {
            // 为特殊字符
            return true;
        }



        // 限制长度 -- 昵称长度不能超过63个字符（来自于数据库字段长度限制）
        if (nickname.length() > 63) {
            return true;
        }
        return false;
    }


    public static boolean isSpecialChar(String str) {
        // 判断是否包含特殊字符
        if (!str.matches("[a-zA-Z0-9\\u4e00-\\u9fa5]+")) {
            // 为特殊字符
            return true;
        }
        return false;
    }

    public static boolean isValidId(String idStr){
        // 判断idStr是否为id
        // 输入的id必须是数字,不能超过int的最大值
        try {
            int id = Integer.parseInt(idStr);
            return id >= 0 && id <= Integer.MAX_VALUE;
        } catch (NumberFormatException e) {
            return false; // 如果转换失败，表示不是一个有效的整数
        }
    }

    public static boolean isValidPrice(String priceStr){
        // 判断priceStr是否为价格
        // 输入的价格必须是数字，小数点后最多两位
        // 不能超过BigDecimal的最大值
        try {
            BigDecimal price = new BigDecimal(priceStr);
            if (price.scale() > 2) {
                return false; // 小数点后最多两位
            }
            return price.compareTo(BigDecimal.ZERO) >= 0 && price.compareTo(new BigDecimal("9999999.99")) <= 0; // 价格的取值范围
        } catch (NumberFormatException e) {
            return false; // 如果转换失败，表示不是一个有效的价格
        }
    }

    public static boolean isValidNumOrLetter(String str) {
        // 判断是否包含数字和英文字母
        // 使用的地方：快递编号
        if (str.matches("[a-zA-Z0-9]+")) {
            // 包含数字和英文字母
            return true;
        }
        return false;
    }


    public static boolean isValidDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false; // 如果转换失败，表示不是一个有效的Double类型
        }
    }
}
