package com.cskaoyan.util;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;

import java.util.Date;

public class InitUtil {
    static {
        //创建一个日期转换器
        DateConverter dateConverter = new DateConverter();
        //设置格式
        dateConverter.setPatterns(new String[]{"yyyy-MM-dd","yyyy-MM-dd HH:mm:ss"});
        //注册格式
        ConvertUtils.register(dateConverter, Date.class);
    }
}
