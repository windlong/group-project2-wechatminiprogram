package com.cskaoyan.util;

import java.math.BigDecimal;

public class SystemConfig {
    public static int getNewLimit() {
        return 4;
    }

    public static int getHotLimit() {
        return 4;
    }

    public static int getBrandLimit() {
        return 4;
    }

    public static int getTopicLimit() {
        return 4;
    }

    public static int getCatlogListLimit() {
        return 4;
    }

    public static int getCatlogMoreLimit() {
        return 4;
    }

    public static Object isAutoCreateShareImage() {
        return false;
    }

    public static BigDecimal getExpressFreightMin() {
        return new BigDecimal(88);
    }

    public static BigDecimal getExpressFreightValue() {
        return new BigDecimal(8);
    }
}
