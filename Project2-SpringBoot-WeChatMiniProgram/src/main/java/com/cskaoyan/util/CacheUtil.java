package com.cskaoyan.util;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class CacheUtil {
    private static Map<String, Object> cache = new ConcurrentHashMap<>();

    public static void save(String key, Object value) {
        cache.put(key, value);
    }

    public static boolean hasCache(String key) {
        return cache.containsKey(key);
    }

    public static Object getCache(String key) {
        return cache.get(key);
    }
}
