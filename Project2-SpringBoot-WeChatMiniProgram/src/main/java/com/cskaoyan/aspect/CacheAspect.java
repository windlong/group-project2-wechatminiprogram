package com.cskaoyan.aspect;

import com.cskaoyan.util.CacheUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;



 import com.cskaoyan.util.CacheUtil;

 import org.aspectj.lang.ProceedingJoinPoint;
 import org.aspectj.lang.Signature;
 import org.aspectj.lang.annotation.Around;
 import org.aspectj.lang.annotation.Aspect;
 import org.aspectj.lang.annotation.Pointcut;
 import org.springframework.stereotype.Component;

// @Component
// @Aspect
// public class CacheAspect {
//
//     @Pointcut("@annotation(com.cskaoyan.anno.Cached) || @target(com.cskaoyan.anno.Cached)")
//     public void cachePointcut() {
//     }
//
//     @Around("cachePointcut()")
//     public Object cache(ProceedingJoinPoint proceedingJoinPoint) {
//         // 获取签名，即类名.方法名
//         Signature signature = proceedingJoinPoint.getSignature();
//         // 获取全部请求参数
//         Object[] args = proceedingJoinPoint.getArgs();
//         // 遍历请求参数
//         for (Object arg : args) {
//             // 如果参数是字符串
//             if (arg instanceof String) {
//                 // 将参数转换为字符串
//                 String str = (String) arg;
//                 /*if (StringUtil.isSensitive(str)) {
//                     // 如果字符串包含敏感词汇，抛出异常
//                     throw new RuntimeException("参数中含有敏感词汇");
//                 }*/
//             }
//         }
//
//         public Object cacheAble (ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//             // 获取方法名
//             String key = signature.getName();
//             // 判断是否有缓存
//             if (CacheUtil.hasCache(key)) {
//                 // 有缓存
//                 return CacheUtil.getCache(key);
//             } else {
//                 Object proceed = null; // 执行的是委托类的方法
//                 try {
//                     proceed = proceedingJoinPoint.proceed();
//                 } catch (Throwable e) {
//                     throw new RuntimeException(e);
//                 }
//                 // 没有缓存，将结果存入缓存
//                 CacheUtil.save(key, proceed);
//                 return proceed;
//             }
//         }
//     }
// }
