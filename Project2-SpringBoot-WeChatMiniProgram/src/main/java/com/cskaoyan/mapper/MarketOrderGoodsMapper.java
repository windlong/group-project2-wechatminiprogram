package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketOrderGoods;
import com.cskaoyan.bean.model.MarketOrderGoodsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketOrderGoodsMapper {
    long countByExample(MarketOrderGoodsExample example);

    int deleteByExample(MarketOrderGoodsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketOrderGoods record);

    int insertSelective(MarketOrderGoods record);

    List<MarketOrderGoods> selectByExample(MarketOrderGoodsExample example);

    MarketOrderGoods selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketOrderGoods record, @Param("example") MarketOrderGoodsExample example);

    int updateByExample(@Param("record") MarketOrderGoods record, @Param("example") MarketOrderGoodsExample example);

    int updateByPrimaryKeySelective(MarketOrderGoods record);

    int updateByPrimaryKey(MarketOrderGoods record);
}