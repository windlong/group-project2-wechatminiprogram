package com.cskaoyan.mapper;

import com.cskaoyan.bean.resp.GoodsStatVo;
import com.cskaoyan.bean.resp.OrderStatVo;
import com.cskaoyan.bean.resp.UserStatVo;

import java.util.List;

/**
 * @Description：
 * @Author：BLUEBLANK
 * @Date：2024/4/12 17:27
 */


public interface MarketStatMapper {

    List<UserStatVo.Row> statUser();
    List<GoodsStatVo.Row> selectGoods();
    List<OrderStatVo.Row> selectOrder();
}
