package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketGoodsAttribute;
import com.cskaoyan.bean.model.MarketGoodsAttributeExample;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketGoodsAttributeMapper {
    long countByExample(MarketGoodsAttributeExample example);

    int deleteByExample(MarketGoodsAttributeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketGoodsAttribute record);

    int insertSelective(MarketGoodsAttribute record);

    List<MarketGoodsAttribute> selectByExample(MarketGoodsAttributeExample example);

    MarketGoodsAttribute selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketGoodsAttribute record, @Param("example") MarketGoodsAttributeExample example);

    int updateByExample(@Param("record") MarketGoodsAttribute record, @Param("example") MarketGoodsAttributeExample example);

    int updateByPrimaryKeySelective(MarketGoodsAttribute record);

    int updateByPrimaryKey(MarketGoodsAttribute record);

    int updateByGoodsId(@Param("id") Integer id, @Param("attribute1") String attribute1, @Param("value") String value, @Param("updateTime") Date updateTime, @Param("addTime") Date addTime, @Param("deleted") Boolean deleted);


    int insertByGoodsId(Integer goodsid, String attribute1, String value, Date updateTime, Date addTime, Boolean deleted);

}