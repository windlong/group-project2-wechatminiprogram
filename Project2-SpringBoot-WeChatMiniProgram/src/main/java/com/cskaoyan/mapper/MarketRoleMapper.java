package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketRole;
import com.cskaoyan.bean.model.MarketRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketRoleMapper {
    long countByExample(MarketRoleExample example);

    int deleteByExample(MarketRoleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketRole record);

    int insertSelective(MarketRole record);

    List<MarketRole> selectByExample(MarketRoleExample example);

    MarketRole selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketRole record, @Param("example") MarketRoleExample example);

    int updateByExample(@Param("record") MarketRole record, @Param("example") MarketRoleExample example);

    int updateByPrimaryKeySelective(MarketRole record);

    int updateByPrimaryKey(MarketRole record);
}