package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketFootprint;
import com.cskaoyan.bean.model.MarketFootprintExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketFootprintMapper {
    long countByExample(MarketFootprintExample example);

    int deleteByExample(MarketFootprintExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketFootprint record);

    int insertSelective(MarketFootprint record);

    List<MarketFootprint> selectByExample(MarketFootprintExample example);

    MarketFootprint selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketFootprint record, @Param("example") MarketFootprintExample example);

    int updateByExample(@Param("record") MarketFootprint record, @Param("example") MarketFootprintExample example);

    int updateByPrimaryKeySelective(MarketFootprint record);

    int updateByPrimaryKey(MarketFootprint record);
}