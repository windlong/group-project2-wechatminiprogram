package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketTopic;
import com.cskaoyan.bean.model.MarketTopicExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketTopicMapper {
    long countByExample(MarketTopicExample example);

    int deleteByExample(MarketTopicExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketTopic record);

    int insertSelective(MarketTopic record);

    List<MarketTopic> selectByExample(MarketTopicExample example);

    MarketTopic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketTopic record, @Param("example") MarketTopicExample example);

    int updateByExample(@Param("record") MarketTopic record, @Param("example") MarketTopicExample example);

    int updateByPrimaryKeySelective(MarketTopic record);

    int updateByPrimaryKey(MarketTopic record);
}