package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketAd;
import com.cskaoyan.bean.model.MarketAdExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketAdMapper {
    long countByExample(MarketAdExample example);

    int deleteByExample(MarketAdExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketAd record);

    int insertSelective(MarketAd record);

    List<MarketAd> selectByExample(MarketAdExample example);

    MarketAd selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketAd record, @Param("example") MarketAdExample example);

    int updateByExample(@Param("record") MarketAd record, @Param("example") MarketAdExample example);

    int updateByPrimaryKeySelective(MarketAd record);

    int updateByPrimaryKey(MarketAd record);
}