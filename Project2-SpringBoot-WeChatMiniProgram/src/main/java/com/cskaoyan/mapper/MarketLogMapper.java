package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketLog;
import com.cskaoyan.bean.model.MarketLogExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketLogMapper {
    long countByExample(MarketLogExample example);

    int deleteByExample(MarketLogExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketLog record);

    int insertSelective(MarketLog record);

    List<MarketLog> selectByExample(MarketLogExample example);

    MarketLog selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketLog record, @Param("example") MarketLogExample example);

    int updateByExample(@Param("record") MarketLog record, @Param("example") MarketLogExample example);

    int updateByPrimaryKeySelective(MarketLog record);

    int updateByPrimaryKey(MarketLog record);
}