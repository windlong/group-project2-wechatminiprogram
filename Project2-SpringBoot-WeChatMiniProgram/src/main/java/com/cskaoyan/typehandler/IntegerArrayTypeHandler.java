package com.cskaoyan.typehandler;

import com.cskaoyan.util.JsonUtil;
import com.cskaoyan.util.StringUtil;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// /admin/admin/option
// /admin/admin/list
@MappedTypes(Integer[].class) // bean类中的成员变量的类型
@MappedJdbcTypes(JdbcType.VARCHAR) // 数据库中的字段的类型
public class IntegerArrayTypeHandler implements TypeHandler<Integer[]> {

    @Override
    public void setParameter(PreparedStatement ps, int index, Integer[] parameter, JdbcType jdbcType)
        throws SQLException {
        String jsonStr = JsonUtil.write(parameter);
        ps.setString(index, jsonStr);
    }

    @Override
    // 从结果集中获取指定列名的字符串
    public Integer[] getResult(ResultSet rs, String columnName) throws SQLException {
        // 获取指定列名的字符串
        String jsonStr = rs.getString(columnName);
        // 如果字符串不为空
        if (StringUtil.isNotEmpty(jsonStr)) {
            // 使用JsonUtil将字符串转换为Integer[]
            Integer[] array = JsonUtil.read(jsonStr, Integer[].class);
            // 返回Integer[]
            return array;
        }
        // 如果字符串为空，返回一个空的Integer[]
        return new Integer[0];

    }

    @Override
    // 从结果集中获取指定列索引的值
    public Integer[] getResult(ResultSet rs, int columnIndex) throws SQLException {
        // 获取指定列索引的值
        String jsonStr = rs.getString(columnIndex);
        // 如果值不为空
        if (StringUtil.isNotEmpty(jsonStr)) {
            // 使用JsonUtil将json字符串转换为Integer数组
            Integer[] array = JsonUtil.read(jsonStr, Integer[].class);
            // 返回Integer数组
            return array;
        }
        // 如果值为空，返回一个空的Integer数组
        return new Integer[0];
    }

    @Override
    public Integer[] getResult(CallableStatement cs, int columnIndex) throws SQLException {
        // 获取CallableStatement中的指定列的值
        String jsonStr = cs.getString(columnIndex);
        // 如果jsonStr不为空
        if (StringUtil.isNotEmpty(jsonStr)) {
            // 使用JsonUtil将jsonStr转换为Integer[]
            Integer[] array = JsonUtil.read(jsonStr, Integer[].class);
            // 返回Integer[]
            return array;
        }
        // 如果jsonStr为空，返回一个空的Integer[]
        return new Integer[0];
    }
}
