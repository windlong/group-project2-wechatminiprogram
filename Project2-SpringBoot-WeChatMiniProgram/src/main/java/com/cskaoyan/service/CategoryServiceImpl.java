package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCategory;
import com.cskaoyan.bean.model.MarketCategoryExample;
import com.cskaoyan.mapper.MarketCategoryMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService{
    @Autowired
    MarketCategoryMapper categoryMapper;
    @Override
    public List queryChannel() {
        MarketCategoryExample example = new MarketCategoryExample();
        example.or().andLevelEqualTo("L1").andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }

    @Override
    public List<MarketCategory> queryL1WithoutRecommend(int page, int limit) {
        MarketCategoryExample example = new MarketCategoryExample();
        example.or().andLevelEqualTo("L1").andNameNotEqualTo("推荐").andDeletedEqualTo(false);
        PageHelper.startPage(page, limit);
        return categoryMapper.selectByExample(example);
    }

    @Override
    public List<MarketCategory> queryByPid(Integer pid) {
        MarketCategoryExample example = new MarketCategoryExample();
        example.or().andPidEqualTo(pid).andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }
}
