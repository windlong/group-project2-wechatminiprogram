package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCart;
import com.cskaoyan.bean.model.MarketCartExample;
import com.cskaoyan.bean.req.CartCheckedVo;
import com.cskaoyan.bean.resp.CartData;
import com.cskaoyan.mapper.MarketCartMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private MarketCartMapper cartMapper;

    @Override
    public CartData index(Integer userId) {
        List<MarketCart> marketCarts = userList(userId);
        return CartData.stat(marketCarts);
    }

    @Override
    public long goodsCount(Integer userId) {
        List<MarketCart> marketCarts = userList(userId);
        long goodsCount = marketCarts.stream().map(MarketCart::getNumber).mapToLong(i -> i).sum();
        return goodsCount;
    }

    @Override
    public MarketCart queryExist(Integer goodsId, Integer productId, Integer userId) {
        MarketCartExample example = new MarketCartExample();
        example.createCriteria().andGoodsIdEqualTo(goodsId)
                .andProductIdEqualTo(productId)
                .andUserIdEqualTo(userId)
                .andDeletedEqualTo(false);

        List<MarketCart> marketCarts = cartMapper.selectByExample(example);

        return marketCarts.size() == 1 ? marketCarts.get(0) : null;
    }

    @Override
    public int create(MarketCart cart) {
        Date current = new Date();
        cart.setAddTime(current);
        cart.setUpdateTime(current);
        int i = cartMapper.insertSelective(cart);
        return i;
    }

    @Override
    public int updateById(MarketCart cart) {
        cart.setUpdateTime(new Date());
        return cartMapper.updateByPrimaryKeySelective(cart);
    }

    @Override
    public int checked(CartCheckedVo cartCheckedVo,Integer userId) {
        Boolean isChecked = cartCheckedVo.getIsChecked();
        List<Integer> productIds = cartCheckedVo.getProductIds();
        // 更新内容
        MarketCart cart = new MarketCart();
        cart.setChecked(isChecked);
        cart.setUpdateTime(new Date());

        // example就是构造条件的(还有排序，去重)
        MarketCartExample example = new MarketCartExample();
        example.createCriteria().andProductIdIn(productIds).andUserIdEqualTo(userId).andDeletedEqualTo(false);
        // cart 提供的是更新的字段，example提供的是更新的条件
        return cartMapper.updateByExampleSelective(cart, example);
    }

    @Override
    public List<MarketCart> findById(Integer cartId, Integer userId) {
        MarketCartExample example = new MarketCartExample();
        MarketCartExample.Criteria criteria = example.createCriteria()
                .andUserIdEqualTo(userId)
                .andDeletedEqualTo(false);
        // 包装类的Integer做值比较 使用intValue做比较
        if (cartId == null || cartId.intValue() == 0) {
            criteria.andCheckedEqualTo(true); // 查询选中的
        } else {
            criteria.andIdEqualTo(cartId);
        }

        List<MarketCart> marketCarts = cartMapper.selectByExample(example);
        return marketCarts;
    }

    private List<MarketCart> userList(Integer userId) {
        MarketCartExample example = new MarketCartExample();
        example.createCriteria().andUserIdEqualTo(userId).andDeletedEqualTo(false);
        List<MarketCart> marketCarts = cartMapper.selectByExample(example);
        return marketCarts;
    }
}
