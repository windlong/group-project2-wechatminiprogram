package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCategory;

import java.util.List;

public interface CategoryService {
    List queryChannel();

    List<MarketCategory> queryL1WithoutRecommend(int page, int limit);

    List<MarketCategory> queryByPid(Integer id);
}
