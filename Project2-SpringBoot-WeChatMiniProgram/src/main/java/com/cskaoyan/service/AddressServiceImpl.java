package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketAddress;
import com.cskaoyan.bean.model.MarketAddressExample;
import com.cskaoyan.mapper.MarketAddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    private MarketAddressMapper addressMapper;
    @Override
    public MarketAddress findById(Integer addressId, Integer userId) {
        MarketAddressExample example = new MarketAddressExample();
        MarketAddressExample.Criteria criteria = example.createCriteria().andUserIdEqualTo(userId).andDeletedEqualTo(false);
        // 如果addressId不为空且不为0，根据当前addressId查询记录
        if (addressId != null && addressId.intValue() != 0) {
            criteria.andIdEqualTo(addressId);
        }else {
            // 如果addressId为空，则查询当前用户的默认的地址
            criteria.andIsDefaultEqualTo(true);
        }
        List<MarketAddress> marketAddresses = addressMapper.selectByExample(example);

        return marketAddresses.size() > 0 ? marketAddresses.get(0) : null;
    }
}
