package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketIssue;

import java.util.List;

public interface IssueService {
    List<MarketIssue> listAll();
}
