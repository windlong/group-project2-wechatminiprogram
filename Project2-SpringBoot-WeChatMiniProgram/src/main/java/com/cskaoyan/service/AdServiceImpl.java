package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketAd;
import com.cskaoyan.bean.model.MarketAdExample;
import com.cskaoyan.mapper.MarketAdMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdServiceImpl implements AdService{
    @Autowired
    MarketAdMapper adMapper;
    @Override
    public List<MarketAd> queryIndex() {
        MarketAdExample example = new MarketAdExample();
        example.or().andPositionEqualTo((byte) 1).andDeletedEqualTo(false).andEnabledEqualTo(true);
        return adMapper.selectByExample(example);
    }
}
