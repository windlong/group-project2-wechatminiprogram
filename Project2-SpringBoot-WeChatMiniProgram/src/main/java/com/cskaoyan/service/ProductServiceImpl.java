package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketGoodsProduct;
import com.cskaoyan.bean.model.MarketGoodsProductExample;
import com.cskaoyan.mapper.MarketGoodsProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements GoodsProductService {
    @Autowired
    private MarketGoodsProductMapper goodsProductMapper;

    @Override
    public List<MarketGoodsProduct> list(Integer id) {
        MarketGoodsProductExample example = new MarketGoodsProductExample();
        example.or().andGoodsIdEqualTo(id).andDeletedEqualTo(false);
        return goodsProductMapper.selectByExample(example);
    }

    @Override
    public MarketGoodsProduct findById(Integer productId) {
        MarketGoodsProductExample example = new MarketGoodsProductExample();
        example.createCriteria().andIdEqualTo(productId).andDeletedEqualTo(false);
        List<MarketGoodsProduct> marketGoodsProducts = goodsProductMapper.selectByExample(example);
        return marketGoodsProducts.size() == 1 ? marketGoodsProducts.get(0) : null;
    }
}
