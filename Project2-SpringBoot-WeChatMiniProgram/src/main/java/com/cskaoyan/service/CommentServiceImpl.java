package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketComment;
import com.cskaoyan.bean.model.MarketCommentExample;
import com.cskaoyan.bean.model.MarketUser;
import com.cskaoyan.mapper.MarketCommentMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    MarketCommentMapper commentMapper;
    @Autowired
    MarketUserMapper userMapper;
    @Override
    public Map goodsComment(Integer goodsId) {
        Map entity = new HashMap();
        MarketCommentExample example = new MarketCommentExample();
        example.createCriteria()
                .andValueIdEqualTo(goodsId)
                .andTypeEqualTo((byte) 0)
                .andDeletedEqualTo(false);
        entity.put("count", commentMapper.countByExample(example));

        PageHelper.startPage(1, 2);
        List<MarketComment> marketComments = commentMapper.selectByExample(example);
        // stream中的 map
        List<Map> data = marketComments.stream().map(comment -> {
            Map map = new HashMap();
            Integer userId = comment.getUserId();
            MarketUser marketUser = userMapper.selectByPrimaryKey(userId);
            map.put("addTime", comment.getAddTime());
            map.put("content", comment.getContent());
            map.put("id", comment.getId());
            map.put("picList", comment.getPicUrls());
            map.put("adminContent", comment.getAdminContent());
            map.put("avatar", marketUser.getAvatar());
            map.put("nickname", marketUser.getNickname());
            return map;
        }).collect(Collectors.toList());
        entity.put("data", data);

        return entity;
    }
}
