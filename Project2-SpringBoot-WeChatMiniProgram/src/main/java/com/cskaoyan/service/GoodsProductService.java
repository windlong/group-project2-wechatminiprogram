package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketGoodsProduct;

import java.util.List;

public interface GoodsProductService {
    List<MarketGoodsProduct> list(Integer id);

    MarketGoodsProduct findById(Integer productId);
}
