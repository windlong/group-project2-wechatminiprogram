package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCart;
import com.cskaoyan.bean.req.CartAddVo;
import com.cskaoyan.bean.req.CartCheckedVo;
import com.cskaoyan.bean.resp.CartData;

import java.util.List;

public interface CartService {
    CartData index(Integer userId);

    long goodsCount(Integer userId);


    MarketCart queryExist(Integer goodsId, Integer productId, Integer userId);

    int create(MarketCart cart);

    int updateById(MarketCart cart);

    int checked(CartCheckedVo cartCheckedVo, Integer userId);

    List<MarketCart> findById(Integer cartId, Integer userId);
}
