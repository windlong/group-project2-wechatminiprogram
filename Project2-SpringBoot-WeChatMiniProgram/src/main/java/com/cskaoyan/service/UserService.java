package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketUser;

public interface UserService {
    MarketUser findById(Integer userId);
}
