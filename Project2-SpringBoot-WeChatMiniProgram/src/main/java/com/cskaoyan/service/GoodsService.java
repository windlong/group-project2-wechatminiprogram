package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCart;
import com.cskaoyan.bean.model.MarketGoods;
import com.cskaoyan.bean.resp.PageVo;

import java.util.List;
import java.util.Map;

public interface GoodsService {
    Map detail(Integer id);
    List queryByNew(int page, int limit);

    List queryByHot(int page, int limit);

    List<MarketGoods> queryByCategory(List<Integer> categoryIds, int page, int limit);

    long count();

    MarketGoods findById(Integer goodsId);

    PageVo goodsRelated(Integer id);

    List<MarketGoods> queryByCart(List<MarketCart> carts);
}
