package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCoupon;

import java.util.List;

public interface CouponService {
    List queryList(int page, int limit);

    List queryAvailableList(Integer userId, int page, int limit);

    MarketCoupon findById(Integer couponId);
}
