package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketUser;
import com.cskaoyan.mapper.MarketUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    MarketUserMapper marketUserMapper;
    @Override
    public MarketUser findById(Integer userId) {
        return marketUserMapper.selectByPrimaryKey(userId);
    }
}
