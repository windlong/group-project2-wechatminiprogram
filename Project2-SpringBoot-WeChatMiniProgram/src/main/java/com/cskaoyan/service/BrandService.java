package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketBrand;

import java.util.List;

public interface BrandService {

    MarketBrand detail(Integer brandId);
    List query(int page, int limit);

}
