package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCoupon;
import com.cskaoyan.bean.model.MarketCouponExample;
import com.cskaoyan.bean.model.MarketCouponUser;
import com.cskaoyan.bean.model.MarketCouponUserExample;
import com.cskaoyan.mapper.MarketCouponMapper;
import com.cskaoyan.mapper.MarketCouponUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CouponUserServiceImpl implements CouponUserService {

    @Autowired
    private MarketCouponUserMapper couponUserMapper;
    @Autowired
    private MarketCouponMapper couponMapper;
    @Override
    public MarketCouponUser findById(Integer userCouponId, Integer userId) {
        Date current = new Date();
        MarketCouponUserExample example = new MarketCouponUserExample();
        example.createCriteria()
                .andUserIdEqualTo(userId) //用户id
                .andIdEqualTo(userCouponId) // 用户领取的优惠券id
                .andStatusEqualTo((short) 0) // 可用状态
                .andStartTimeLessThan(current) // 大于开始时间
                .andEndTimeGreaterThan(current) // 小于结束时间
                .andDeletedEqualTo(false); // 逻辑删除
        List<MarketCouponUser> marketCouponUsers = couponUserMapper.selectByExample(example);
        return marketCouponUsers.size() > 0 ? marketCouponUsers.get(0) : null;
    }

    @Override
    public List<MarketCouponUser> list(BigDecimal totalPrice, Integer userId) {
        Date current = new Date();
        MarketCouponUserExample example = new MarketCouponUserExample();
        example.createCriteria()
                .andUserIdEqualTo(userId) //用户id
                .andStatusEqualTo((short) 0) // 可用状态
                .andStartTimeLessThan(current) // 大于开始时间
                .andEndTimeGreaterThan(current) // 小于结束时间
                .andDeletedEqualTo(false); // 逻辑删除
        List<MarketCouponUser> marketCouponUsers = couponUserMapper.selectByExample(example);
        if (marketCouponUsers == null || marketCouponUsers.size() == 0) {
            return new ArrayList<>();
        }


        Map<Integer, List<MarketCouponUser>> couponIdMap = marketCouponUsers.stream().collect(Collectors.groupingBy(MarketCouponUser::getCouponId));
        MarketCouponExample example2 = new MarketCouponExample();
        example2.createCriteria().andIdIn(couponIdMap.keySet().stream().toList())
                .andMinLessThan(totalPrice)
                .andStatusEqualTo((short) 0)
                .andDeletedEqualTo(false);
        List<MarketCoupon> couponList = couponMapper.selectByExample(example2);
        List<MarketCouponUser> result = couponList.stream().sorted(Comparator.comparing(MarketCoupon::getDiscount).reversed())
                .map(marketCoupon ->
                        couponIdMap.get(marketCoupon.getId())
                ).flatMap(List::stream).collect(Collectors.toList());
        return result.size() > 0 ? result : new ArrayList<>();
    }
}
