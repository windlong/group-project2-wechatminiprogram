package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCategory;
import com.cskaoyan.bean.model.MarketGoods;
import com.cskaoyan.util.SystemConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

@Service
public class IndexServiceImpl implements IndexService{
    @Autowired
    AdService adService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    CouponService couponService;
    @Autowired
    GoodsService goodsService;
    @Autowired
    BrandService brandService;
    @Autowired
    TopicService topicService;
    ExecutorService executorService = Executors.newFixedThreadPool(10);

    @Override
    public Map index(Integer userId) {
        Map<String, Object> entity = new HashMap<>();
        // 之前后台管理中的广告管理
        Callable<List> bannerListCallable = () -> adService.queryIndex();
        // 之前后台管理中的类目管理
        Callable<List> channelListCallable = () -> categoryService.queryChannel();
        // 获取优惠券列表
        Callable<List> couponListCallable;
        // 判断是否是登录状态
        if (userId == null) {
            //如果是未登录，就查询所有的优惠券中的前三条
            couponListCallable = () -> couponService.queryList(1, 3);
        } else {
            //如果是已经登录，就查询当前用户还未领取的前三张优惠券
            couponListCallable = () -> couponService.queryAvailableList(userId, 1, 3);
        }

// 查询新添加的商品Goods，之后后台管理中的商品管理，商品上架时有选项是否是新品，根据添加时间做排序，limit获取market_system表中的值
        Callable<List> newGoodsListCallable = () -> goodsService.queryByNew(1, SystemConfig.getNewLimit());
// 查询热门商品Goods，之后后台管理中的商品管理，商品上架时有选项是否是热卖，根据添加时间做排序，limit获取market_system表中的值
        Callable<List> hotGoodsListCallable = () -> goodsService.queryByHot(1, SystemConfig.getHotLimit());
// 之后后台管理中的制造商管理，根据添加时间做排序，limit获取market_system表中的值
        Callable<List> brandListCallable = () -> brandService.query(1, SystemConfig.getBrandLimit());
// 之前后台管理中的专题管理，根据添加时间做排序，limit获取market_system表中的值
        Callable<List> topicListCallable = () -> topicService.queryList(1, SystemConfig.getTopicLimit());

// 查询类目category以及类目下的商品goods，limit获取market_system表中的值
        Callable<List> floorGoodsListCallable = this::getCategoryList;

        FutureTask<List> bannerTask = new FutureTask<>(bannerListCallable);
        FutureTask<List> channelTask = new FutureTask<>(channelListCallable);
        FutureTask<List> couponListTask = new FutureTask<>(couponListCallable);
        FutureTask<List> newGoodsListTask = new FutureTask<>(newGoodsListCallable);
        FutureTask<List> hotGoodsListTask = new FutureTask<>(hotGoodsListCallable);
        FutureTask<List> brandListTask = new FutureTask<>(brandListCallable);
        FutureTask<List> topicListTask = new FutureTask<>(topicListCallable);
        FutureTask<List> floorGoodsListTask = new FutureTask<>(floorGoodsListCallable);

        executorService.submit(bannerTask);
        executorService.submit(channelTask);
        executorService.submit(couponListTask);
        executorService.submit(newGoodsListTask);
        executorService.submit(hotGoodsListTask);
        executorService.submit(brandListTask);
        executorService.submit(topicListTask);
        executorService.submit(floorGoodsListTask);
        try {
            entity.put("banner", bannerTask.get());
            entity.put("channel", channelTask.get());
            entity.put("couponList", couponListTask.get());
            entity.put("newGoodsList", newGoodsListTask.get());
            entity.put("hotGoodsList", hotGoodsListTask.get());
            entity.put("brandList", brandListTask.get());
            entity.put("topicList", topicListTask.get());
            entity.put("floorGoodsList", floorGoodsListTask.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }
    private List<Map> getCategoryList() {
        List<Map> categoryList = new ArrayList<>();
        List<MarketCategory> catL1List = categoryService.queryL1WithoutRecommend(1, SystemConfig.getCatlogListLimit());
        for (MarketCategory catL1 : catL1List) {
            List<MarketCategory> catL2List = categoryService.queryByPid(catL1.getId());
            List<Integer> l2List = new ArrayList<>();
            for (MarketCategory catL2 : catL2List) {
                l2List.add(catL2.getId());
            }

            List<MarketGoods> categoryGoods;
            if (l2List.size() == 0) {
                categoryGoods = new ArrayList<>();
            } else {
                categoryGoods = goodsService.queryByCategory(l2List, 1, SystemConfig.getCatlogMoreLimit());
            }

            Map<String, Object> catGoods = new HashMap<>();
            catGoods.put("id", catL1.getId());
            catGoods.put("name", catL1.getName());
            catGoods.put("goodsList", categoryGoods);
            categoryList.add(catGoods);
        }
        return categoryList;
    }
}
