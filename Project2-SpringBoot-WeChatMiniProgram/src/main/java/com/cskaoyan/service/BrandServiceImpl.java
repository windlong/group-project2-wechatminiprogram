package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketBrand;
import com.cskaoyan.bean.model.MarketBrandExample;
import com.cskaoyan.mapper.MarketBrandMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    MarketBrandMapper brandMapper;


    @Override
    public MarketBrand detail(Integer brandId) {
        return brandMapper.selectByPrimaryKey(brandId);
    }
    public List<MarketBrand> query(Integer page, Integer limit, String sort, String order) {
        MarketBrandExample example = new MarketBrandExample();
        example.or().andDeletedEqualTo(false);
        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }
        PageHelper.startPage(page, limit);
        return brandMapper.selectByExample(example);
    }

    @Override
    public List<MarketBrand> query(int page, int limit) {
        return query(page, limit, null, null);
    }
}
