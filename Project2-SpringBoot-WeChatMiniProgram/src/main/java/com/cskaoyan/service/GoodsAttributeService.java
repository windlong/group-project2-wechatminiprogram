package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketGoodsAttribute;

import java.util.List;

public interface GoodsAttributeService {
    List<MarketGoodsAttribute> list(Integer id);

}
