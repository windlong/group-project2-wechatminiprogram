package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCollectExample;
import com.cskaoyan.mapper.MarketCollectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CollectServiceImpl implements CollectService{
    @Autowired
    private MarketCollectMapper collectMapper;
    @Override
    public int userHasCollect(Integer id) {
        MarketCollectExample example = new MarketCollectExample();
        example.createCriteria().andIdEqualTo(id).andDeletedEqualTo(false);
        long userHasCollect = collectMapper.countByExample(example);
        return (int) userHasCollect;
    }
}
