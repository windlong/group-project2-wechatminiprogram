package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketIssue;
import com.cskaoyan.bean.model.MarketIssueExample;
import com.cskaoyan.mapper.MarketIssueMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IssueServiceImpl implements IssueService{
    @Autowired
    MarketIssueMapper issueMapper;
    @Override
    public List<MarketIssue> listAll() {
        MarketIssueExample example = new MarketIssueExample();
        example.createCriteria().andDeletedEqualTo(false);
        return issueMapper.selectByExample(example);
    }
}
