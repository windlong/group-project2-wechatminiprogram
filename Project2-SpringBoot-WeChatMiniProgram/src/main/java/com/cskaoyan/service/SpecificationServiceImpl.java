package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketGoodsSpecification;
import com.cskaoyan.bean.model.MarketGoodsSpecificationExample;
import com.cskaoyan.mapper.MarketGoodsSpecificationMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SpecificationServiceImpl implements GoodsSpecificationService {
    @Autowired
    private MarketGoodsSpecificationMapper goodsSpecificationMapper;

    @Override
    public Object list(Integer id) {
        List<MarketGoodsSpecification> goodsSpecificationList = queryByGid(id);

        Map<String, VO> map = new HashMap<>();
        List<VO> specificationVoList = new ArrayList<>();

        for (MarketGoodsSpecification goodsSpecification : goodsSpecificationList) {
            String specification = goodsSpecification.getSpecification();
            VO goodsSpecificationVo = map.get(specification);
            if (goodsSpecificationVo == null) {
                goodsSpecificationVo = new VO();
                goodsSpecificationVo.setName(specification);
                List<MarketGoodsSpecification> valueList = new ArrayList<>();
                valueList.add(goodsSpecification);
                goodsSpecificationVo.setValueList(valueList);
                map.put(specification, goodsSpecificationVo);
                specificationVoList.add(goodsSpecificationVo);
            } else {
                List<MarketGoodsSpecification> valueList = goodsSpecificationVo.getValueList();
                valueList.add(goodsSpecification);
            }
        }

        return specificationVoList;
    }

    private List<MarketGoodsSpecification> queryByGid(Integer id) {
        MarketGoodsSpecificationExample example = new MarketGoodsSpecificationExample();
        example.createCriteria().andGoodsIdEqualTo(id).andDeletedEqualTo(false);
        List<MarketGoodsSpecification> marketGoodsSpecifications = goodsSpecificationMapper.selectByExample(example);
        return marketGoodsSpecifications;
    }
    @Data
    private class VO {
        private String name;
        private List<MarketGoodsSpecification> valueList;
    }
}
