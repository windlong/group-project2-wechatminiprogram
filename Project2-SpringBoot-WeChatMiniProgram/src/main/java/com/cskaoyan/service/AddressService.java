package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketAddress;

public interface AddressService {
    MarketAddress findById(Integer addressId, Integer userId);
}
