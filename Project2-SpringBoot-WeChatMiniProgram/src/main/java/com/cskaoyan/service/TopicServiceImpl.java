package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketTopic;
import com.cskaoyan.bean.model.MarketTopicExample;
import com.cskaoyan.mapper.MarketTopicMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService{
    @Autowired
    MarketTopicMapper topicMapper;
    public List<MarketTopic> queryList(int page, int limit) {
        return queryList(page, limit, "add_time", "desc");
    }

    public List<MarketTopic> queryList(int page, int limit, String sort, String order) {
        MarketTopicExample example = new MarketTopicExample();
        example.or().andDeletedEqualTo(false);
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, limit);
        return topicMapper.selectByExample(example);
    }
}
