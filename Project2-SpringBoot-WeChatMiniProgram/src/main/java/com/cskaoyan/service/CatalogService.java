package com.cskaoyan.service;

import com.cskaoyan.bean.resp.CatalogIndexVo;
import com.cskaoyan.bean.resp.CurrentCatalogVo;

public interface CatalogService {
    CurrentCatalogVo current(Integer id);

    CatalogIndexVo index();
}
