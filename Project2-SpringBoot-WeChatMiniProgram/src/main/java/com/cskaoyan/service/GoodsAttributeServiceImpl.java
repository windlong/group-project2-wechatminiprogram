package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketGoodsAttribute;
import com.cskaoyan.bean.model.MarketGoodsAttributeExample;
import com.cskaoyan.mapper.MarketGoodsAttributeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsAttributeServiceImpl implements GoodsAttributeService {
    @Autowired
    MarketGoodsAttributeMapper goodsAttributeMapper;
    @Override
    public List<MarketGoodsAttribute> list(Integer id) {
        MarketGoodsAttributeExample example = new MarketGoodsAttributeExample();
        example.createCriteria().andGoodsIdEqualTo(id).andDeletedEqualTo(false);
        List<MarketGoodsAttribute> marketGoodsAttributes = goodsAttributeMapper.selectByExample(example);
        return marketGoodsAttributes;
    }
}
