package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketCouponUser;

import java.math.BigDecimal;
import java.util.List;

public interface CouponUserService {
    MarketCouponUser findById(Integer userCouponId, Integer userId);

    List<MarketCouponUser> list(BigDecimal totalPrice, Integer userId);
}
